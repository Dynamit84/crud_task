var express = require('express'),
    router = express.Router(),
    Customer = require('../model/db'),
    bodyParser = require('body-parser');

router.use(bodyParser.json());

router.get('/customers', function (req, res) {
    Customer.find(function (err, docs) {
        res.send(docs);
    })
});

router.post('/customers', function (req, res) {
    var customer = new Customer(req.body);
    customer.save(function (err, doc) {
        res.send(doc);
    })
});

router.delete('/customers/:id', function (req, res) {
    Customer.findByIdAndRemove(req.params.id, function (err, doc) {
        res.send(doc);
    })
});

router.put('/customers/:id', function (req, res) {
    Customer.findOneAndUpdate({_id: req.params.id},
        req.body, function (err, doc) {
            res.send(doc)
        })
});

module.exports = router;