define(['jquery', 'underscore', 'backbone', 'message'], function ($, _, Backbone, Message) {

    return Backbone.Collection.extend({
        model: Message
    })
});