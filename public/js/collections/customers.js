define(['jquery', 'underscore', 'backbone', 'customer'], function ($, _, Backbone, Customer) {

    return Backbone.Collection.extend({
        model: Customer,
        url: 'http://localhost:3000/customers',
        initialize: function () {
            this.fetch({reset: true})
                .done(Backbone.trigger.bind(Backbone,'infoMsg', 'READ'))
                .fail(Backbone.trigger.bind(Backbone, 'infoMsg', 'READ'));

        }
    })
});