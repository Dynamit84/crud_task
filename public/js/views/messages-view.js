define(['jquery', 'underscore', 'backbone', 'message-view', 'message', 'text!../templates/span.html'], function ($, _, Backbone, MessageView, Message, tmpl) {

    return Backbone.View.extend({
        el: 'ul',
        crud: '',
        className: 'msg-list',
        initialize: function () {
            this.listenTo(this.collection, 'add', this.onAdd)
                .listenTo(Backbone, 'infoMsg', this.logInfo)
                .listenTo(Backbone, 'clearLog', this.clearLog);
        },
        onAdd: function (model) {
            this.$el.append(new MessageView({model: model}).render().el).find('span').addClass(this.crud);
        },
        logInfo: function (crud, res, operMsg) {
            var logMsg = new Message ({
                    msg: this.getMsg(crud, res, operMsg)
                });
            this.crud = crud.toLowerCase();
            this.collection.add(logMsg);
        },
        getMsg: function (crud, res, operMsg) {
            var sucsMsg = '', errMsg = '',
                str = _.template(tmpl)({crud: crud});

            if(res instanceof Array) {
                if(res.length == 0) {
                   sucsMsg = 'Database is EMPTY, please add new customers!'
                } else {
                    sucsMsg = 'Successfully performed ' + str + ' operation for ' + res.length + ' customer(s)';
                    errMsg = 'Failed to ' + str + ' information';
                }
            }
            else {
                sucsMsg = 'Successfully performed ' + str + ' operation for customer ' + res.name.first + ' ' + res.name.last;
                errMsg = 'Failed to ' + str + ' customer';
            }

            return (operMsg === 'success') ? sucsMsg : errMsg;
        },
        clearLog: function () {
            this.collection.reset();
            this.$el.empty();
        }
    })
});