define(['jquery', 'underscore', 'backbone', 'customerView', 'customer', 'messages', 'messages-view'], function ($, _, Backbone, CustomerView, Customer, Messages, MessagesView) {

    return Backbone.View.extend({
        el: 'body',
        $cacheElems: {},
        initialize: function () {
            this.cacheElements();
            new MessagesView ({
                collection: new Messages ()
            });
            this.listenTo(this.collection, 'reset', this.onReset)
                .listenTo(this.collection, 'add', this.onAdd);
        },
        events: {
            'click .add-customer': 'addCustomer',
            'change .file-input': 'onFileSelect',
            'click .upload': 'getFile',
            'click .clear': 'onClear',
            'blur input': 'fieldValidate'
        },
        fieldValidate: function (event, elem) {
            var evTar = (event) ? event.target : elem,
                $evTar = $(evTar);

            if(!evTar.validity.valid) {
                $evTar.addClass('error');
                $evTar.attr('placeholder', evTar.validationMessage);
            }
        },
        onAdd: function (model) {
            this.$cacheElems.$tbody.append(new CustomerView({model: model}).render().el)
        },
        onReset: function () {
            this.collection.each(this.onAdd, this);
        },
        addCustomer: function (event) {
            var customer = {
                name: {
                    first: this.$cacheElems.$first.val(),
                    last: this.$cacheElems.$last.val()
                },
                dateOfBirth: new Date(this.$cacheElems.$dateOfBirth.val()),
                companyName: this.$cacheElems.$companyName.val(),
                phone: {
                    cell: this.$cacheElems.$cell.val(),
                    work: this.$cacheElems.$work.val()
                },
                skype: this.$cacheElems.$skype.val()
            };

            event.preventDefault();
            if(this.$cacheElems.$form[0].checkValidity()) {
                this.saveToDB(customer);
            }
            else {
                this.showFieldToFill();
            }
        },
        showFieldToFill: function () {
            var inputs = this.$cacheElems.$form.find('input');
            _.each(inputs, function (elem) {
                this.fieldValidate(null, elem);
            }, this);
        },
        cacheElements: function () {
            this.$cacheElems = {
                $log: this.$('.log'),
                $tbody: this.$('tbody'),
                $form: this.$('.form'),
                $first: this.$('[name=first]'),
                $last: this.$('[name=last]'),
                $dateOfBirth: this.$('[name=date-of-birth]'),
                $companyName: this.$('[name=company]'),
                $cell: this.$('[name=cell-phone]'),
                $work: this.$('[name=work-phone]'),
                $skype: this.$('[name=skype]'),
                $file: this.$('[type=file]')
            }
        },
        onClear: function () {
            Backbone.trigger('clearLog');
        },
        getFile: function (event) {
            event.preventDefault();
            this.$cacheElems.$file.click();
        },
        onFileSelect: function (event) {
            var file = event.target.files[0],
                reader;

            if(file.type !== 'application/json') {
                console.log('No JSON file had been chosen!')
            }
            else {
                reader = new FileReader();
                reader.onload = function (event) {
                    var resultData = JSON.parse(event.target.result);
                    _.each(resultData, this.saveToDB, this);
                }.bind(this);
                reader.readAsText(file);
            }
        },
        saveToDB: function (obj) {
            var customer = new Customer (obj);
            this.collection.add(customer);
            customer
                .save(null)
                .done(Backbone.trigger.bind(Backbone, 'infoMsg', 'CREATE'))
                .fail(Backbone.trigger.bind(Backbone, 'infoMsg', 'CREATE'))
        }
    })
});