define(['jquery', 'underscore', 'backbone', 'text!../templates/log-msg.html'], function ($, _, Backbone, tmpl) {

    return Backbone.View.extend({
        tagName: 'li',
        render: function() {
            this.$el.html(_.template(tmpl)(this.model.toJSON()));

            return this;
        }
    })
});