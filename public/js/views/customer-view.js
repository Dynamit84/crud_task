define(['jquery', 'underscore', 'backbone', 'text!../templates/customer.html'], function ($, _, Backbone, tmpl) {

    return Backbone.View.extend({
        tagName: 'tr',
        className: "customer",
        events: {
            'click .del-customer': 'onDelete',
            'blur td': 'dataChange'
        },
        render: function() {
            this.$el.html(_.template(tmpl)(this.model.toJSON()));

            return this;
        },
        onDelete: function () {
            this.model.destroy()
                                .done(this.someFunc.bind(this))
                                .fail(Backbone.trigger.bind(Backbone, 'infoMsg', 'DELETE'));
        },
        someFunc: function () {
            Backbone.trigger('infoMsg', 'DELETE', arguments[0], arguments[1]);
            this.$el.remove();
        },
        dataChange: function (event) {
            var evTarg = event.target,
                tdContent = evTarg.textContent,
                tdClass = evTarg.className,
                modelAttr = this.classToModelAttr(tdClass),
                modelAttrVal = '';

            if(typeof modelAttr === 'string') {
                modelAttrVal = this.model.get(modelAttr);
                if(modelAttrVal !== tdContent) {
                    this.model.set(modelAttr, tdContent);
                    this.saveChanges();
                }
            }
            else {
                modelAttrVal = this.model.get(modelAttr[0])[modelAttr[1]];
                if(modelAttrVal !== tdContent) {
                    var updateAttr = _.clone(this.model.get(modelAttr[0]));
                    updateAttr[modelAttr[1]] = tdContent;
                    this.model.set(modelAttr[0], updateAttr);
                    this.saveChanges();
                }
            }
        },
        saveChanges: function () {
            this.model.save(null)
                .done(Backbone.trigger.bind(Backbone, 'infoMsg', 'UPDATE'))
                .fail(Backbone.trigger.bind(Backbone, 'infoMsg', 'UPDATE'));
        },
        classToModelAttr: function(tdClass){
            var classToArray = tdClass.split('-');

            return classToArray.reduce(function(prev, curr, ind, arr) {
                var modelAttr;

                if(typeof this.model.get(prev) === 'object') {
                    modelAttr = arr;
                }
                else {
                    modelAttr = prev + curr.charAt(0).toUpperCase() + curr.substr(1);
                }

                return modelAttr;
            }.bind(this));

        }
    })
});