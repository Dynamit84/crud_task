requirejs.config({
    paths: {
        "jquery": "../libs/jquery.min",
        "underscore": "../libs/underscore-min",
        "backbone": "../libs/backbone-min",
        "text": "../libs/text",
        "person":"models/person",
        "customer":"models/customer",
        "customers": "collections/customers",
        "customerView": "views/customer-view",
        "allCustomersView": "views/all-customers-view",
        "message": "models/message",
        "message-view": "views/message-view",
        "messages": "collections/messages",
        "messages-view": "views/messages-view"
    }
});

require(['jquery', 'underscore', 'backbone', 'customers', 'allCustomersView'], function ($, _, Backbone, Customers, CustomersView) {
    new CustomersView({
        collection: new Customers()
    });

});