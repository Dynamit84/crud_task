define(['jquery', 'underscore', 'backbone', 'person'], function ($, _, Backbone, Person) {

    return Person.extend({
        companyName: '',
        phone: {
            cell: '',
            work: ''
        },
        skype: ''
    })
});