define(['jquery', 'underscore', 'backbone'], function ($, _, Backbone) {
    Backbone.Model.prototype.idAttribute = '_id';
    return Backbone.Model.extend({
            name: {
                first: '',
                last: ''
            },
            dateOfBirth: ''
    })
});